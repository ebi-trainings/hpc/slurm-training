## Cron jobs
  
### Task 01: Create a cron job that submits an sbatch one-liner

Instructions:

1. using `scrontab`, create a cron job that runs every two minutes
2. This cron submits an sbatch one-liner that simply prints the hostname of the allocated compute node
3. Check the status of your jobs and the pending reason
4. Make sure to disable your cron job when you're done. 