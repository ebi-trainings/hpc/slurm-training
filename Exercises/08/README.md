## Job Arrays
  
### Task 01: Create a batch script that submits a job array

Instructions:

1. Create a batch script that submits a job array of 10 array members
2. Each of these members should sleep for 60 seconds then print the task ID and the job ID
3. Make sure that no more than 3 array members can run at the same time
4. Check the job status and the output files