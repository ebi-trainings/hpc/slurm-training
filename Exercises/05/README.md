**Exercise #5:** 



**Instructions:**
Make sure all of your submissions use the account `training-oct-24` and the reservation `training-oct-24`
1. Submit an sbatch one-liner requesting 1 CPU for 2 hours
2. Submit another sbatch one-liner requesting 16 CPUs
3. Submit a third sbatch one-liner requesting 1 CPU for 10 minutes. The job will mainly run `sleep 600` command
4. List your existing jobs
5. Observe the pending reasons for some of your jobs
6. Cancel all of your jobs in pending state using a single command
7. Cancel your running job (if any) using the job ID
8. Using sacct, List all the jobs you submitted in the last 24 hours
9. Use the job script generator at https://slurm-jsg.ebi.ac.uk to generate a new batch script. Explore the different available options.