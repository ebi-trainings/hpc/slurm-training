**Exercise #2: Submitting an interactive SLURM job using salloc**

**Objective:** Submit an interactive SLURM job using `salloc` and execute some commands within the allocated compute resources.

**Instructions:**
1. Use `salloc` to allocate resources from the "training-oct-24" reservation and "training-oct-24" account with a wall time of 2 minutes and 2GB of memory.
2. After allocation, run some simple commands like `hostname`, `uptime`, or `ls` within the allocated resources.
3. Confirm that the job runs successfully.
