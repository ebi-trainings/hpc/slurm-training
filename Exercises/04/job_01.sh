#!/bin/bash

#SBATCH -J SleeperJob
#SBATCH --mem=2G
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH -t 00:05:00
#SBATCH --reservation=training-oct-24
#SBATCH --account=training-oct-24

echo "This job does nothing and sleep for 30seconds"
sleep 30

