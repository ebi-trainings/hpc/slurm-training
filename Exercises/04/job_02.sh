#!/bin/bash
  
#SBATCH -J MatrixMul
#SBATCH --mem=8G
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH -t 00:05:00
#SBATCH --reservation=training-oct-24
#SBATCH --account=training-oct-24


module load openmpi
gcc -fopenmp matrix_mul.cpp
./a.out
