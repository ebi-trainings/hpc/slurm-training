## Submit batch jobs and read performance data

In this exercise we will submit two different batch jobs. Then we will read job performance data using SLURM Commands and also get a report over email when job ends

### Task 01: Submit Job 'Job_01.sh'.
- Inspect the job and you will notice the job script contains sleep statement which is to emulate a low performing job.
- Submit this job by issuing `sbatch job_01.sh` and note down the JOB ID.
- Now issue `seff <job_id>` and evaluate the output.
- A sample output from `seff`

```bash
Job ID: 34977224
Cluster: codon
User/Group: arif/systems
State: COMPLETED (exit code 0)
Nodes: 1
Cores per node: 2
CPU Utilized: 00:00:00
CPU Efficiency: 0.00% of 00:01:00 core-walltime
Job Wall-clock time: 00:00:30
Memory Utilized: 396.00 KB
Memory Efficiency: 0.02% of 2.00 GB
```

### Task 02: Submit Job 'Job_02.sh'
- Inspect the job script and add SLURM Directive to get an email alert when job finishes.
- Submit modified job script by issuing `sbatch job_02.sh`
- Once job completes you will get a Email notification which includes job performance data
- Evaluate job performance data by issuing `seff <job_id>`
- You must have recieved an email once this job has finished. Evaluate the performance data provided in the email.



